import React from "react";
import Case from "../components/Case";
export default function Home() {
    return (
        <Case>
            <div className="w-full max-w-lg">
                <h4 className="text-2xl">
                    Hello React{" "}
                    <button class="inline-block cursor-pointer rounded-md bg-gray-800 px-4 py-3 text-center text-sm font-semibold uppercase text-white transition duration-200 ease-in-out hover:bg-gray-900">
                        Button
                    </button>
                </h4>
                <p className="text-lg leading-relaxed text-gray-400">
                    A JavaScript library for building user interfaces
                </p>
            </div>
        </Case>
    );
}
